package com.example.mvvm_example.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}